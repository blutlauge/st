# st - simple terminal

st is a simple terminal emulator for X which sucks less. This is my personal 
build based on version 0.9 which includes the following patches:

* [alpha](https://st.suckless.org/patches/alpha)
* [anysize](https://st.suckless.org/patches/anysize)
* [blinking cursor](https://st.suckless.org/patches/blinking_cursor)
* [boxdraw](https://st.suckless.org/patches/boxdraw)
* [copyurl multiline](https://st.suckless.org/patches/copyurl)
* [glyph wide support for boxdraw](https://st.suckless.org/patches/glyph)
* [scrollback](https://st.suckless.org/patches/scrollback)
* [scrollback-mouse](https://st.suckless.org/patches/scrollback)
* [xresources with signal reloading](https://st.suckless.org/patches/xresources-with-reload-signal)
 

## Requirements

In order to build st you need the Xlib header files.


## Installation

Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

    make clean install


## Running st

If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.


## Credits

Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.

